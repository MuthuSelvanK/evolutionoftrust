import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class MachineTest {
    Machine machine = new Machine();

    @Test
    public void testPlayer1ScoreIfBothCoop(){
        Integer expectedPlayer1Score = 2;
        Map<String,Integer> actual = machine.calculateScore(MoveType.COOPERATE,MoveType.COOPERATE);
        Assert.assertEquals(actual.get("player1"),expectedPlayer1Score);
    }


    @Test
    public void testPlayer1ScoreIfBothCheat(){
        Integer expectedPlayer1Score = 0;
        Map<String,Integer> actual = machine.calculateScore(MoveType.CHEAT,MoveType.CHEAT);
        Assert.assertEquals(actual.get("player1"),expectedPlayer1Score);
    }

    @Test
    public void testPlayer1ScoreIfPlayer1CooperatePlayer2Cheat(){
        Integer expectedPlayer1Score = -1;
        Map<String,Integer> actual = machine.calculateScore(MoveType.COOPERATE,MoveType.CHEAT);
        Assert.assertEquals(actual.get("player1"),expectedPlayer1Score);
    }

    @Test
    public void testPlayer1ScoreIfPalyer1CheatPlayer2Coop(){
        Integer expectedPlayer1Score = 3;
        Map<String,Integer> actual = machine.calculateScore(MoveType.CHEAT,MoveType.COOPERATE);
        Assert.assertEquals(actual.get("player1"),expectedPlayer1Score);
    }
}
