import java.util.HashMap;
import java.util.Map;

public class Machine {

    public Map<String, Integer> calculateScore(MoveType choice1, MoveType choice2) {
        Map<String, Integer> map = new HashMap<>();

        if(choice1==MoveType.CHEAT && choice2 == MoveType.CHEAT) {
            map.put("player1", 0);
            map.put("player2", 0);
        }
        else if(choice1==MoveType.COOPERATE   && choice2 == MoveType.COOPERATE) {
            map.put("player1", 2);
            map.put("player2", 2);
        }
        else if(choice1==MoveType.COOPERATE && choice2 == MoveType.CHEAT) {
            map.put("player1", -1);
            map.put("player2", 3);
        }
        else{
            map.put("player1", 3);
            map.put("player2", -1);
        }
        return map;
    }
}
