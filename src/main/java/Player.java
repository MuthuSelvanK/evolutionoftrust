import java.util.Scanner;

public class Player {
    private int score;
    private MoveType choice;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public MoveType getChoice() {
        return choice;
    }

    public void setChoice(MoveType choice) {
        Scanner scanner = new Scanner(System.in);
        if(scanner.nextInt() == 1)
            this.choice = MoveType.COOPERATE;
        else
            this.choice = MoveType.CHEAT;
    }


}
